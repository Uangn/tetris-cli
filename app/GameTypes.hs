module GameTypes
    ( Position, RelPosition
    , Pos4, RPos4
    , addPos, subPos
    , rotateVec
    , PieceType (..)
    , Direction (..)
    , RotationDirection (..)
    ) where

{- Extern -}
import qualified Data.Vector as V

{- Positions -}
type Position = (Int, Int)
type RelPosition = (Int, Int)

type RPos4 = V.Vector RelPosition
type Pos4 = V.Vector Position

{- Position Utils -}
addPos :: Position -> Position -> Position
addPos (x1,y1) (x2,y2) = (x1+x2, y1+y2)

subPos :: Position -> Position -> Position
subPos (x1,y1) (x2,y2) = (x1-x2, y1-y2)

rotateVec :: V.Vector a -> V.Vector a
rotateVec xs = (V.tail xs) <> (V.singleton $ V.head xs)

{- Pieces -}
data PieceType = T
               | I
               | O
               | S
               | Z
               | J
               | L



{- Direction -}

data Direction = LeftSide | RightSide | Up | Down
    deriving (Eq, Show)


data RotationDirection = CW | CCW
    deriving (Eq, Show)
