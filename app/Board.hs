module Board
    ( board_width, board_height, board_visual_height
    , isPosInBoard
    , initPieceOrigin -- Might not need in interface
    ) where

{- Intern -}
import GameTypes

{- Constants -}
board_width :: Int
board_width = 10

board_height :: Int
board_height = 40

board_visual_height :: Int
board_visual_height = 20

{- Initial States -}
initPieceOrigin :: Position
initPieceOrigin = (board_width `div` 2, board_visual_height)

{- Checks -}
isPosInBoard :: Position -> Bool
isPosInBoard (x,y) = 
    0 <= x && x <= board_width - 1 &&
    0 <= y && y <= board_height - 1
