module PieceRotate
    ( rotate
    ) where

{- Extern -}
import qualified Data.Vector as V

{- Intern -}
import GameTypes
import Constants
import Piece

rotate :: RotationDirection -> Piece -> Maybe Piece
rotate dir piece = (Just . newPiece) =<< tryRot dir piece
    where
    newPiece (newOrigin, newRPos4) = Piece
        { pieceOrigin = newOrigin
        , rpos4 = newRPos4
        }

tryRot :: RotationDirection -> Piece -> Maybe (Position, V.Vector RelPosition)
tryRot dir (Piece origin r4) = tryRot' 4 r4
    where
    tryRot' 0 _ = Nothing
    tryRot' n x =
        if isPos4InBoard origin rotated
            then Just (currNewOrigin, subtractHeadFromPos4 unrotated)
            else tryRot' (n-1) $ rotateVec x
        where
        currHead = V.head x
        currNewOrigin = (addPos origin
                      . (addPos $ V.head unrotated) $ currHead)
        rotated = rotatePiece dir
                . subtractHeadFromPos4
                $ x
        unrotated = iterate rotateVec rotated !! (n `mod` 4)
        subtractHeadFromPos4 ys = fmap (flip subPos $ V.head ys) ys

rotatePiece :: RotationDirection -> V.Vector RelPosition -> V.Vector RelPosition
rotatePiece dir = fmap newPoint
    where
    newPoint (x,y) =
        (-y * sin90
        , x * sin90)

    sin90
        | dir == CW = -1
        | otherwise = 1
