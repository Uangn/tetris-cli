module Piece
    ( Piece (..)
    , rpos4TOpos4
    , isPos4InBoard, isPieceInBoard
    , moveTo, moveVec, fall, strafe
    ) where

{- Extern -}
import qualified Data.Vector as V
{- Intern -}
import GameTypes
import Constants
import Board (isPosInBoard)

data Piece = Piece 
    { pieceOrigin :: Position
    , rpos4 :: RPos4
    } deriving (Show)



{- Type Conversions -}
rpos4TOpos4 :: Position -> RPos4 -> Pos4
rpos4TOpos4 center r4 = fmap (addPos center) r4



{- Check Piece -}
isPos4InBoard :: Position -> RPos4 -> Bool
isPos4InBoard origin r4 = not . V.elem False . fmap isPosInBoard $ rpos4TOpos4 origin r4

isPieceInBoard :: Piece -> Bool
isPieceInBoard (Piece origin r4) = isPos4InBoard origin r4



{- Movement -}
moveTo :: Position -> Piece -> Maybe Piece
moveTo new_origin p@(Piece _ r4)
    | isPieceInBoard p = Just newPiece
    | otherwise      = Nothing
    where
    newPiece = Piece
        { pieceOrigin = new_origin
        , rpos4 = r4
        }


moveVec :: RelPosition -> Piece -> Maybe Piece
moveVec (dx,dy) p@(Piece (ox,oy) r4)
    | isPieceInBoard p = Just newPiece
    | otherwise      = Nothing
    where
    newPiece = Piece
        { pieceOrigin = (ox+dx, oy+dy)
        , rpos4 = r4
        }


fall :: Piece -> Maybe Piece
fall = moveVec (0,-1)


strafe :: Direction -> Piece -> Maybe Piece
strafe LeftSide  = moveVec (-1,0)
strafe RightSide = moveVec (1,0)
strafe e         = error $ "Not a valid direction " ++ show e

-- quick_drop
-- soft_drop
