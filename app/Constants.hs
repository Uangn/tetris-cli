module Constants 
    ( initPieceRPos4
    ) where

{- Extern -}
import qualified Data.Vector as V

{- Intern -}
import GameTypes


initPieceRPos4 :: PieceType -> RPos4
initPieceRPos4 T = V.fromList [(0,0), (-1, 0), (1, 0), ( 0, 1)]
initPieceRPos4 I = V.fromList [(0,0), ( 0, 1), (0,-1), ( 0,-2)]
initPieceRPos4 O = V.fromList [(0,0), ( 1, 0), (1,-1), ( 0,-1)]
initPieceRPos4 S = V.fromList [(0,0), (-1, 0), (0, 1), ( 1, 1)]
initPieceRPos4 Z = V.fromList [(0,0), ( 1, 0), (0, 1), (-1, 1)]
initPieceRPos4 J = V.fromList [(0,0), ( 0,-1), (0, 1), (-1,-1)]
initPieceRPos4 L = V.fromList [(0,0), ( 0,-1), (0, 1), ( 1,-1)]
